# st



## About st

Simple Terminal or Suckless Terminal with some patches added with a dracula theme!

## Dependencies

JetBrainsMono Nerd Font

## Installation

git clone https://gitlab.com/PissedJojo/st.git

cd st

make

sudo make install

## Patches added

st-alpha
st-bold-is-not-bright
st-font2
st-gradient
st-rightclickpaste
st-scrollback
st-boxdraw

## License

this is under the GNU GENERAL PUBLIC LICENSE Version2
